/**
 * @file pullrequest 自动cherrypick到指定分支
 * @author xuexb <fe.xiaowu@gmail.com>
 */
 const util=require('util')
 const child_process = require('child_process')
 const exec=util.promisify(child_process.exec)
 const { getPkgConfig } = require('../../utils')
 const {
     pullRequestlabel
    } = require('../../github')
  
 function autoCherry (on) {
   on('pull_request_closed', async({ payload, repo }) => {
       console.log("pr关闭")
       console.log(payload)
       const has=payload.pull_request.merge_commit_sha
       if(has){
          const labels=await pullRequestlabel(payload)
          console.log('输出labels')
          console.log(labels)
          for(label in labels){
            if(labels[label].name.indexOf("cherry-pick ")!=-1){
              console.log(labels[label].name)
              var branch=labels[label].name.slice(labels[label].name.indexOf("cherry-pick")+12)
              console.log(branch)
              var branchs='git checkout -b '+branch+' origin/'+branch
              var cherr='git cherry-pick --allow-empty -m1 '+has
              await exec('cd cherry_test\n'+'git pull\n'+branchs+'\n'+cherr+'\n'+'git push')
            
          }
      
        }
       }
     }
   )
 }
 
 module.exports = autoCherry
 