/**
 * @file PR 提示标题正确性
 * @author xuexb <fe.xiaowu@gmail.com>
 */

const format = require('string-template')
const { getPkgCommitPrefix } = require('../../utils')
const {
  commentPullRequest,
  addLabelsToPullRequest,
  removeLabelsToPullRequest,
  pullRequestHasLabel
} = require('../../github')
const actions = getPkgCommitPrefix()
//1表示正确 
//2表示被删除
//3表示未修改  
const docmatch = content =>{
  if (content.indexOf("### Documentation")!=-1)
  {
    if(content.indexOf("### Does this pull request potentially affect one of the following parts:\r\n\r\n*If `yes` was chosen, please highlight the changes*\r\n\r\n  - Dependencies (does it add or upgrade a dependency): (yes / no)\r\n  - The public API: (yes / no)\r\n  - The schema: (yes / no / don\'t know)\r\n  - The default values of configurations: (yes / no)\r\n  - The wire protocol: (yes / no)\r\n  - The rest endpoints: (yes / no)\r\n  - The admin cli options: (yes / no)\r\n  - Anything that affects deployment: (yes / no / don\'t know)\r\n\r\n### Documentation\r\n  \r\n\r\nNeed to update docs? Check the box below:\r\n- [ ] doc-required (if the answer is yes)\r\n- [ ] no-need-doc (if the answer is no)\r\n- [ ] doc (if this PR contains only doc changes)")!=-1)
      return 3
    else
      return 1
    }

  else
    return 2
}
const match = title => {
  console.log(actions.some(action => title.indexOf(`${action}:`) === 0))
  console.log('fffffffffffffffffffffffffff')
  return actions.some(action => title.indexOf(`${action}:`) === 0)
}
const commentSuccess = [
  '@{user}:Thanks for providing doc info!'
].join('')

const docError1 = [
  '@{user}:Thanks for your contribution. For this PR, do we need to update docs?\n(The [PR template contains info about doc](https://github.com/apache/pulsar/blob/master/.github/PULL_REQUEST_TEMPLATE.md#documentation), which helps others know more about the changes. Can you provide doc-related info in this and future PR descriptions? Thanks)'
].join('')
const docError2 = [
  '@{user}:Thanks for your contribution. For this PR, do we need to update docs?\n(The [PR template contains info about doc](https://github.com/apache/pulsar/blob/master/.github/PULL_REQUEST_TEMPLATE.md#documentation), which helps others know more about the changes. Can you provide doc-related info in this and future PR descriptions? Thanks) '
].join('')
module.exports = on => {
  if (actions.length) {
    on('pull_request_opened', async({ payload, repo }) => {
      var doc=docmatch(payload.pull_request.body)
      console.log(payload.pull_request)
      if(await pullRequestHasLabel(payload,'doc') || await pullRequestHasLabel(payload,'doc-required') || await pullRequestHasLabel(payload,'no-doc-need')){
        doc=1
      }
      console.log(doc)
      if(doc==2){
        commentPullRequest(
          payload,
          format(docError1, {
            user: payload.pull_request.user.login
          })
        )
        addLabelsToPullRequest(payload,'doc-info-missing')
      }
      if(doc==3){
        commentPullRequest(
          payload,
          format(docError2, {
            user:payload.pull_request.user.login
          })
        )
    
        addLabelsToPullRequest(payload,'doc-info-missing')
      }
    
    })
    on('pull_request_edited', async ({ payload, repo }) => {
      var doc=docmatch(payload.pull_request.body)
      if(await pullRequestHasLabel(payload,'doc') || await pullRequestHasLabel(payload,'doc-required') || await pullRequestHasLabel(payload,'no-doc-need')){
        doc=1
      }
      if (doc==1 && await pullRequestHasLabel(payload, 'doc-info-missing')) {
        commentPullRequest(
          payload,
          format(commentSuccess, {
            user: payload.pull_request.user.login
          })
        )

        removeLabelsToPullRequest(payload, 'doc-info-missing')
      }
    })
    on('pull_request_labeled', async ({ payload, repo }) => {
      var doc=docmatch(payload.pull_request.body)
      if(await pullRequestHasLabel(payload,'doc') || await pullRequestHasLabel(payload,'doc-required') || await pullRequestHasLabel(payload,'no-doc-need')){
        doc=1
      }
      if (doc==1 && await pullRequestHasLabel(payload, 'doc-info-missing')) {
        commentPullRequest(
          payload,
          format(commentSuccess, {
            user: payload.pull_request.user.login
          })
        )

        removeLabelsToPullRequest(payload, 'doc-info-missing')
      }
    })
  }
}
