
 const {
  issueHasLabel,
  addLabelsToIssue,
  removeLabelsToIssue,
  pullRequestlabel
} = require('../../github')

const handle = async ({ payload, repo }) => {
  console.log('开始读取评论')
  const action = payload.comment.body
  if(action){
    if(action.indexOf('/help')!=-1){
      const exist = await issueHasLabel(payload,'help')
      if(!exist){
        addLabelsToIssue(payload,'help')
      }
    }
    if(action.indexOf('/remove-help')!=-1){
      const exist = await issueHasLabel(payload,'help')
      if(exist){
        removeLabelsToIssue(payload,"help")
      }
    }
    if(action.indexOf('/good-first-issue')!=-1){
      const exist = await issueHasLabel(payload,'good-first-issue')
      if(!exist){
        addLabelsToIssue(payload,'good-first-issue')
      }
    }
    if(action.indexOf('/remove-good-first-issue')!=-1){
      const exist = await issueHasLabel(payload,'good-first-issue')
      if(exist){
        removeLabelsToIssue(payload,'good-first-issue')
      }  
    }  

  }
}
module.exports = on => {
  on('issue_comment_created', handle)
}