/**
 * @file pullrequest 自动 `assign` 给指定人员
 * @author xuexb <fe.xiaowu@gmail.com>
 */

 const { getPkgConfig } = require('../../utils')
 const { addAssigneesToPullRequest } = require('../../github')
  
 function autoAssign (on) {
   on('pull_request_opened', ({ payload, repo }) => {
       addAssigneesToPullRequest(
         payload,
         payload.pull_request.user.login
       )
     }
   )
 }
 
 module.exports = autoAssign
 