/**
 * @file PR 标题自动打标签
 * @author xuexb <fe.xiaowu@gmail.com>
 */

const {
  addLabelsToPullRequest,
  pullRequestHasLabel
} = require('../../github')

const getAction = body => {
  console.log(body.indexOf("- [x] ")+6)
  console.log(body.indexOf('(',(body.indexOf("- [x] "))))
  return (body.slice(body.indexOf("- [x] ")+6,body.indexOf(' ',(body.indexOf("- [x] ")+7))))
}

const ACTION_TO_LABEL_MAP = [
  'doc','doc-required','no-need-doc'

]
const prlabel=','+ACTION_TO_LABEL_MAP.join(",")+","
const handle = async ({ payload, repo }) => {
  console.log('修改标签')
  const action = getAction(payload.pull_request.body)
  console.log(action)
  if (action  && (prlabel.indexOf(','+action+',')!=-1)){
    const exist = await pullRequestHasLabel(payload, action)
    if (!exist) {
      addLabelsToPullRequest(payload, action)
    }
  }
}

module.exports = on => {
  on('pull_request_edited', handle)
  on('pull_request_opened', handle)
}
