# github-bot

github 机器人：在服务端上启动一个基于 [koajs](http://koajs.com/) 的 http server ，建立一些项目的规范（如 issue 格式、 pull request 格式、配置一些指定 label 根据的 owner 、统一 git commit log 格式等），基于 [github webhooks](https://developer.github.com/webhooks/) 和 [github api](https://developer.github.com/v3/) 让机器人（通常是一个单独的帐号，如 [@jiandansousuo-bot](https://github.com/jiandansousuo-bot) ）自动处理一些事情，从而达到快速响应、自动化、解放人力的效果。

[![Build Status](https://travis-ci.org/xuexb/github-bot.svg?branch=master)](https://travis-ci.org/xuexb/github-bot)
[![js-standard-style](https://img.shields.io/badge/code%20style-standard-brightgreen.svg)](http://standardjs.com)
[![Test Coverage](https://img.shields.io/coveralls/xuexb/github-bot/master.svg)](https://coveralls.io/r/xuexb/github-bot?branch=master)

## 声明

该 [仓库@xuexb/github-bot](https://github.com/xuexb/github-bot/) 是用来演示 github-bot 的基本功能，因为具体需要实现的功能，可能因项目而不同，如果你需要她，你可以 fork 并相应的添加、删除功能。以下功能是一些常用的 show case 。

## 功能 - Feature


### Pull Request

- [x] 发 PR 时根据打的 label 自动添加指定的 reviewer ，需要配置 `package.json` 中 `config.github-bot.labelToAuthor` 映射 - [示例](https://github.com/xuexb/github-bot/pull/33#event-1320253347)
- [x] 发 PR 时根据是否有文档描述进行提醒
- [x] 发 PR 时根据描述为pr打上标签
- [x] 读取评论中的bot命令进行操作，主要为help和good-first-issue
## 如何使用

### 1. 创建 access tokens

<https://github.com/settings/tokens> （_需要在 .env 里配置_）

### 2. 创建 webhook

https://github.com/用户名/项目名/settings/hooks/new

- Payload URL: www.example.com:8000
- Content type: application/json
- trigger: Send me everything.
- Secret: xxx （_需要在 .env 里配置_）

### 3. 开发运行

```bash
npm install
cp env .env
vim .env
npm start
```

### 4. 部署

本项目使用 [pm2](https://github.com/Unitech/pm2) 进行服务管理，发布前请先全局安装 [pm2](https://github.com/Unitech/pm2)

```bash
npm install pm2 -g
npm run deploy
```

后台启动该服务后，可以通过 `pm2 ls` 来查看服务名称为 `github-bot` 的运行状态。具体 [pm2](https://github.com/Unitech/pm2) 使用，请访问：https://github.com/Unitech/pm2

### 5. 日志系统说明

本系统 `logger` 服务基于 [log4js](https://github.com/log4js-node/log4js-node)。
在根目录的 `.env` 文件中有个参数 `LOG_TYPE` 默认为 `console`，参数值说明：

```
console - 通过 console 输出log。
file - 将所有相关log输出到更根目录的 `log` 文件夹中。
```
